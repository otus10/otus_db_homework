﻿
using Infrastructure.Interfaces.DataAccess;
using UseCases.Requests;
using Infrastructure.Implementation.DataAccess.Postgre.Sberbank;
using Infrastructure.Interfaces.Providers;
using Infrastructure.Implementation.Providers.Console;
using System;
using UseCases.Interfaces;
using UseCases.Commands;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace otus_db_homework
{
   class Program
   {
      private static IConfiguration _configuration;
      private static IDbContext _dbContext;
      private static IOutputProvider _outputProvider;
      public static async Task Main(string[] args)
      {
         Configure(args);

         IRequest getAllTablesData = new GetAllTablesDataRequest(_dbContext, _outputProvider);
         await getAllTablesData.ExecuteAsync();

         AddCardOperationDto addCardOperationDto = new AddCardOperationDto { CardId = -1, Sum = 500m };
         ICommand addCardOperation = new AddCardOperationCommand(addCardOperationDto, _dbContext);
         await addCardOperation.ExecuteAsync();

         IRequest getCardOperations = new GetCardOperationsRequest(_dbContext, _outputProvider);
         await getCardOperations.ExecuteAsync();
         Console.ReadKey();
      }
      private static void Configure(string[] args)
      {
         _configuration = new ConfigurationBuilder()
             .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
             .AddJsonFile("appsettings.json", false)
             .Build();

         SberbankDbContextFactory sberbankDbContextFactory = new SberbankDbContextFactory(_configuration);
         _dbContext = sberbankDbContextFactory.CreateDbContext(args);
         _outputProvider = new ConsoleProvider();
      }

   }
}
