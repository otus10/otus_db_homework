﻿using Infrastructure.Interfaces.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Infrastructure.Implementation.Providers.Console
{
   public class ConsoleProvider : IOutputProvider
   {
      public void Output<T>(T input)
      {
         string inputSerialize =  JsonSerializer.Serialize<T>(input, new JsonSerializerOptions() { WriteIndented = true });
         System.Console.WriteLine(inputSerialize);
      }
   }
}
