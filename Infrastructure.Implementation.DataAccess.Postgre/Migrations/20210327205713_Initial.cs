﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Infrastructure.Implementation.DataAccess.Postgre.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Persons",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    LastName = table.Column<string>(type: "text", nullable: true),
                    FirstName = table.Column<string>(type: "text", nullable: true),
                    BirthDay = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Persons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Cards",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PersonId = table.Column<int>(type: "integer", nullable: false),
                    Number = table.Column<string>(type: "text", nullable: true),
                    ExpireMonth = table.Column<string>(type: "text", nullable: true),
                    ExpireYear = table.Column<string>(type: "text", nullable: true),
                    CVV = table.Column<int>(type: "integer", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cards", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cards_Persons_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Persons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CardOperations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CardId = table.Column<int>(type: "integer", nullable: false),
                    Sum = table.Column<decimal>(type: "numeric", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CardOperations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CardOperations_Cards_CardId",
                        column: x => x.CardId,
                        principalTable: "Cards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Persons",
                columns: new[] { "Id", "BirthDay", "DateCreated", "FirstName", "LastName" },
                values: new object[,]
                {
                    { -1, new DateTime(1983, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 3, 27, 23, 57, 12, 630, DateTimeKind.Local).AddTicks(2312), "Vasiliy", "Pupkin" },
                    { -2, new DateTime(1985, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 3, 27, 23, 57, 12, 631, DateTimeKind.Local).AddTicks(6360), "Ivan", "Ivanov" },
                    { -3, new DateTime(2000, 10, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 3, 27, 23, 57, 12, 631, DateTimeKind.Local).AddTicks(6382), "Aleksey", "Grigoriev" },
                    { -4, new DateTime(1988, 4, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 3, 27, 23, 57, 12, 631, DateTimeKind.Local).AddTicks(6385), "Maksim", "Sharikov" },
                    { -5, new DateTime(1996, 7, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 3, 27, 23, 57, 12, 631, DateTimeKind.Local).AddTicks(6387), "Anton", "Pushkin" }
                });

            migrationBuilder.InsertData(
                table: "Cards",
                columns: new[] { "Id", "CVV", "DateCreated", "ExpireMonth", "ExpireYear", "Number", "PersonId" },
                values: new object[,]
                {
                    { -1, 290, new DateTime(2021, 3, 27, 23, 57, 12, 633, DateTimeKind.Local).AddTicks(1773), "12", "2027", "5555 5555 5555 5555", -1 },
                    { -2, 112, new DateTime(2021, 3, 27, 23, 57, 12, 633, DateTimeKind.Local).AddTicks(4787), "11", "2025", "5555 5555 5555 6666", -2 },
                    { -3, 911, new DateTime(2021, 3, 27, 23, 57, 12, 633, DateTimeKind.Local).AddTicks(4797), "09", "2026", "5555 5555 5555 7777", -3 },
                    { -4, 356, new DateTime(2021, 3, 27, 23, 57, 12, 633, DateTimeKind.Local).AddTicks(4800), "07", "2022", "5555 5555 5555 8888", -4 },
                    { -5, 566, new DateTime(2021, 3, 27, 23, 57, 12, 633, DateTimeKind.Local).AddTicks(4801), "01", "2023", "5555 5555 5555 9999", -5 }
                });

            migrationBuilder.InsertData(
                table: "CardOperations",
                columns: new[] { "Id", "CardId", "DateCreated", "Sum" },
                values: new object[,]
                {
                    { -1, -1, new DateTime(2021, 3, 27, 23, 57, 12, 633, DateTimeKind.Local).AddTicks(5651), 10m },
                    { -2, -1, new DateTime(2021, 3, 27, 23, 57, 12, 633, DateTimeKind.Local).AddTicks(6786), 20m },
                    { -3, -1, new DateTime(2021, 3, 27, 23, 57, 12, 633, DateTimeKind.Local).AddTicks(6794), -5m },
                    { -4, -2, new DateTime(2021, 3, 27, 23, 57, 12, 633, DateTimeKind.Local).AddTicks(6796), 105m },
                    { -5, -2, new DateTime(2021, 3, 27, 23, 57, 12, 633, DateTimeKind.Local).AddTicks(6797), -30m }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CardOperations_CardId",
                table: "CardOperations",
                column: "CardId");

            migrationBuilder.CreateIndex(
                name: "IX_Cards_PersonId",
                table: "Cards",
                column: "PersonId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CardOperations");

            migrationBuilder.DropTable(
                name: "Cards");

            migrationBuilder.DropTable(
                name: "Persons");
        }
    }
}
