﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Implementation.DataAccess.Postgre.Sberbank
{
   internal static class SberbankDataSeed
   {
      public static void Seed(this ModelBuilder builder)
      {
         builder.Entity<Person>().HasData
         (
             new Person { Id = -1, FirstName = "Vasiliy", LastName = "Pupkin", BirthDay = new DateTime(1983, 12, 12) },
             new Person { Id = -2, FirstName = "Ivan", LastName = "Ivanov", BirthDay = new DateTime(1985, 01, 01) },
             new Person { Id = -3, FirstName = "Aleksey", LastName = "Grigoriev", BirthDay = new DateTime(2000, 10, 11) },
             new Person { Id = -4, FirstName = "Maksim", LastName = "Sharikov", BirthDay = new DateTime(1988, 04, 06) },
             new Person { Id = -5, FirstName = "Anton", LastName = "Pushkin", BirthDay = new DateTime(1996, 07, 12) }
         );

         builder.Entity<Card>().HasData
         (
             new Card { Id = -1, PersonId = -1, ExpireYear = "2027", ExpireMonth = "12", CVV = 290, Number = "5555 5555 5555 5555" },
             new Card { Id = -2, PersonId = -2, ExpireYear = "2025", ExpireMonth = "11", CVV = 112, Number = "5555 5555 5555 6666" },
             new Card { Id = -3, PersonId = -3, ExpireYear = "2026", ExpireMonth = "09", CVV = 911, Number = "5555 5555 5555 7777" },
             new Card { Id = -4, PersonId = -4, ExpireYear = "2022", ExpireMonth = "07", CVV = 356, Number = "5555 5555 5555 8888" },
             new Card { Id = -5, PersonId = -5, ExpireYear = "2023", ExpireMonth = "01", CVV = 566, Number = "5555 5555 5555 9999" }
         );

         builder.Entity<CardOperation>().HasData
         (
             new CardOperation { Id = -1, CardId = -1, Sum = 10 },
             new CardOperation { Id = -2, CardId = -1, Sum = 20 },
             new CardOperation { Id = -3, CardId = -1, Sum = -5 },
             new CardOperation { Id = -4, CardId = -2, Sum = 105 },
             new CardOperation { Id = -5, CardId = -2, Sum = -30 }
         );
      }
   }
}
