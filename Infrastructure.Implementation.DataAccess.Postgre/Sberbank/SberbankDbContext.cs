﻿using Infrastructure.Interfaces.DataAccess;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Implementation.DataAccess.Postgre.Sberbank
{
   public class SberbankDbContext : DbContext, IDbContext
   {
      public DbSet<Person> Persons { get; set; }

      public DbSet<Card> Cards { get; set; }

      public DbSet<CardOperation> CardOperations { get; set; }

      public SberbankDbContext(DbContextOptions<SberbankDbContext> options) : base(options)
      {
      }
      public async Task<int> SaveChangesAsync()
      {
         return await base.SaveChangesAsync();
      }
      protected override void OnModelCreating(ModelBuilder modelBuilder)
      {
         base.OnModelCreating(modelBuilder);
         modelBuilder.Seed();
      }
   }
}
