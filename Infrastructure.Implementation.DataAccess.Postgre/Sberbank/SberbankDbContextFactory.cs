﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Infrastructure.Implementation.DataAccess.Postgre.Sberbank
{
   public class SberbankDbContextFactory : IDesignTimeDbContextFactory<SberbankDbContext>
   {
      private readonly IConfiguration _configuration;

      public SberbankDbContextFactory()
      {
         _configuration = new ConfigurationBuilder()
                              .SetBasePath(Directory.GetCurrentDirectory())
                              .AddJsonFile("migrationsettings.json")
                              .Build();
      }
      public SberbankDbContextFactory(IConfiguration configuration)
      {
         _configuration = configuration;
      }
      public SberbankDbContext CreateDbContext(string[] args)
      {
         DbContextOptionsBuilder<SberbankDbContext> optionsBuilder = new DbContextOptionsBuilder<SberbankDbContext>();
         optionsBuilder.UseNpgsql(_configuration.GetConnectionString("Postgre")
                                 , assembly => assembly.MigrationsAssembly(typeof(SberbankDbContext).Assembly.GetName().Name)
                                 );
         return new SberbankDbContext(optionsBuilder.Options);
      }
   }
}
