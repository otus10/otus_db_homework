﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interfaces.Providers
{
   public interface IOutputProvider
   {
      public void Output<T>(T input);
   }
}
