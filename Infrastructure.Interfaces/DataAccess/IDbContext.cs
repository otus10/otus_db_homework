﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Interfaces.DataAccess
{
   public interface IDbContext
   {
      DbSet<Person> Persons { get; }
      DbSet<Card> Cards { get; }
      DbSet<CardOperation> CardOperations { get; }
      Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
      int SaveChanges();
   }
}
