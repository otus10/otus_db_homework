﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UseCases.Commands
{
   public class AddCardOperationDto
   {
      public int CardId { get; set; }

      public decimal Sum { get; set; }

      public static explicit operator CardOperation(AddCardOperationDto addCardOperationDto)
      {
         return new CardOperation
         {
            Id = 0,
            Sum = addCardOperationDto.Sum,
            CardId = addCardOperationDto.CardId
         };
      }
      public static implicit operator AddCardOperationDto(CardOperation cardOperation)
      {
         return new AddCardOperationDto
         {
            Sum = cardOperation.Sum,
            CardId = cardOperation.CardId
         };
      }
   }
}
