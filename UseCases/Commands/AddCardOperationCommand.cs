﻿using Entities.Models;
using Infrastructure.Interfaces.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseCases.Interfaces;


namespace UseCases.Commands
{
   public class AddCardOperationCommand : ICommand
   {
      private readonly IDbContext _dbContext;
      private readonly AddCardOperationDto _addCardOperationDto;

      public AddCardOperationCommand(AddCardOperationDto addCardOperationDto, IDbContext dbContext)
      {
         _addCardOperationDto = addCardOperationDto;
         _dbContext = dbContext;
      }

      public async Task ExecuteAsync()
      {
         CardOperation cardOperation = (CardOperation)_addCardOperationDto;
         _dbContext.CardOperations.Add(cardOperation);
         _ = await _dbContext.SaveChangesAsync();
      }
   }
}
