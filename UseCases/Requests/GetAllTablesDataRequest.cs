﻿using Infrastructure.Interfaces.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Interfaces.Providers;
using Entities.Models;
using UseCases.Interfaces;
using System.Threading.Tasks;

namespace UseCases.Requests
{
   public class GetAllTablesDataRequest : IRequest
   {
      private readonly IDbContext _dbContext;
      private readonly IOutputProvider _outputProvider;

      public GetAllTablesDataRequest(IDbContext dbContext, IOutputProvider outputProvider)
      {
         _dbContext = dbContext;
         _outputProvider = outputProvider;
      }

      public async Task ExecuteAsync()
      {
         List<Person> persons = await _dbContext.Persons.ToListAsync();
         _outputProvider.Output<string>(nameof(_dbContext.Persons).ToString());
         _outputProvider.Output<List<Person>>(persons);

         List<Card> cards = await _dbContext.Cards.ToListAsync();
         _outputProvider.Output<string>(nameof(_dbContext.Cards).ToString());
         _outputProvider.Output<List<Card>>(cards);

         GetCardOperationsRequest getCardOperations = new GetCardOperationsRequest(_dbContext, _outputProvider);
         await getCardOperations.ExecuteAsync();
      }
   }
}
