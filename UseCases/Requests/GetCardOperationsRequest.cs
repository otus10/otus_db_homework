﻿using Entities.Models;
using Infrastructure.Interfaces.DataAccess;
using Infrastructure.Interfaces.Providers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseCases.Interfaces;

namespace UseCases.Requests
{
   public class GetCardOperationsRequest : IRequest
   {
      private readonly IDbContext _dbContext;
      private readonly IOutputProvider _outputProvider;

      public GetCardOperationsRequest(IDbContext dbContext, IOutputProvider outputProvider)
      {
         _dbContext = dbContext;
         _outputProvider = outputProvider;
      }

      public async Task ExecuteAsync()
      {
         List<CardOperation> cardOperations = await _dbContext.CardOperations.ToListAsync();
         _outputProvider.Output<string>(nameof(_dbContext.CardOperations).ToString());
         _outputProvider.Output<List<CardOperation>>(cardOperations);
      }
   }
}
