﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Entities.Models
{
   public class Card : BaseEntity
   {
      [JsonIgnore]
      public Person Person { get; set; }
      public int PersonId { get; set; }

      public string Number { get; set; }

      public string ExpireMonth { get; set; }

      public string ExpireYear { get; set; }

      public int CVV { get; set; }
      [JsonIgnore]
      public List<CardOperation> CardOperations { get; set; }

   }
}
