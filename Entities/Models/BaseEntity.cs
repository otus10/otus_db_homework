﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Entities.Models.Interfaces;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
   public class BaseEntity : IBaseEntity
   {
      [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
      public int Id { get; set; }
      public DateTime DateCreated { get; set; } = DateTime.Now;
   }
}
