﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Entities.Models
{
   public class CardOperation: BaseEntity
   {
      [JsonIgnore]
      public Card Card { get; set; }

      public int CardId { get; set; }

      public decimal Sum { get; set; }
   }
}
