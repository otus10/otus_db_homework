﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Entities.Models
{
   public class Person : BaseEntity
   {
      public string LastName { get; set; }

      public string FirstName { get; set; }

      public DateTime BirthDay { get; set; }

      [JsonIgnore]
      public List<Card> Cards{ get; set; }
   }
}
